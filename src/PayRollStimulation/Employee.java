/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package PayRollStimulation;

/**
 *
 * @author sahej
 */
/*
* To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 *
 *
 * @author sahej
 */

public class Employee {

private String  name;
private double wage;
private int hours;

public Employee (String n, double w, int h)

{
name = n;
wage = w;
hours = h;
}

public String getName()
{

return this.name;
}

public double getWage()
{

return this.wage;
}
public int getHours()
{

return this.hours;
}
public void setName(String n)
{
name= n;

}
public void setWage(Double w)
{
wage= w;

}
public  void setHours(int h )
{
hours= h;

}
public double calculatePay()
{

return hours*wage;

} 

 class Manager extends Employee
{

private double bonus;

public Manager( String name , double wage, int hours ,double b)
{

super(name,wage,hours);
bonus = b;
}
public double getBonus()
{

return this.bonus;
}
public void setBonus(Double b)
{
bonus= b;

}

@Override
public double calculatePay()
{

return super.calculatePay()+bonus;

}
 }

}



