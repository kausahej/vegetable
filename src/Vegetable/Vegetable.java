/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Vegetable;

/**
 *
 * @author sahej
 */
/*
* To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 *
 *
 * @author sahej
 */

public abstract class Vegetable {
    
    private final String colour; 
    private final double size; 
    
    public Vegetable(String colour, double size) {
        this.colour = colour;
        this.size = size;
    }

    
    public String getColour() {
        return colour;
    }

    
    public double getSize() {
        return size;
    }

    
    public abstract boolean isRipe();
}
 class Beet extends Vegetable {
    
    public Beet(String colour, double size) {
        super(colour, size);
    }

    
    @Override
    public boolean isRipe() {
       
        return (getSize() >= 2.0 && "red".equalsIgnoreCase(getColour()));
    }
   public class Carrot extends Vegetable {
  
    public Carrot(String colour, double size) {
        super(colour, size);
    }

    @Override
    public boolean isRipe() {

        
        return (getSize() >= 1.5 && "orange".equalsIgnoreCase(getColour()));
}
   }
 }