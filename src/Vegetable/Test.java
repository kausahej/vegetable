/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package Vegetable;
import Vegetable.Beet.Carrot;
import java.util.ArrayList;


/**
 *
 * @author sahej
 */
public class Test {
    public static void main(String[] args) {
        
        ArrayList<Vegetable> vegetables = new ArrayList<>();
        
        Beet b1 = new Beet("red", 2.0);
        
        Beet b2 = new Beet("yellow", 1.0);
       
        Carrot c1 =  b1.new Carrot("orange", 1.5);
        Carrot c2 = b2.new Carrot("green", 1.0);
        vegetables.add(b1);
        vegetables.add(b2);
        vegetables.add(c1);
        vegetables.add(c2);

       
        vegetables.stream().map(vegetable -> {
            System.out.println("Vegetable Name: " + vegetable.getClass().getName());
            return vegetable;
        }).map(vegetable -> {
            System.out.println("Vegetable Colour: " + vegetable.getColour());
            return vegetable;
        }).map(vegetable -> {
            System.out.println("Vegetable Size: " + vegetable.getSize());
            return vegetable;
        }).map(vegetable -> {
            System.out.println("Riped or Not ? " + vegetable.isRipe());
            return vegetable;
        }).forEachOrdered(_item -> {
            System.out.println();
        });
    }
}